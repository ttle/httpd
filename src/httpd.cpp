/**********************************************************
*
*Creator    : Thong LT
*Email      : letrthong@gmail.com
*Website : http://letrthong.blogspot.com
*
***********************************************************/
#include "WebServer.h"

int main()
{

    WebServer *pWeb = new WebServer();
    SocketConfig *pConfig = new SocketConfig();

    if(pConfig != NULL && pConfig->loadConfiguration("./config/config.ini") == 0 )
    {
        pWeb->setConfigFile(pConfig);
        delete pConfig;
    }else
    {
        //Http Protocol
        pWeb->setLocalPort(80);
        pWeb->setWebDirPath("./www/");
    }

    pWeb->start();
    return 0;
}
