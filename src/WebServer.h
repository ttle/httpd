/************************************************
*
*Creator : Thong LT
*Email   : letrthong@gmail.com
*Website : http://letrthong.blogspot.com
*
*************************************************/
#ifndef WEB_SERVER_H_
#define WEB_SERVER_H_

#include <string>

#include <webservice/socketServer.h>
#include <http/httpRequest.h>
#include <http/httpResponse.h>

class  WebServer :public socketServer
{
public:
    WebServer();
    ~WebServer();

    int doGet(httpRequest Req, httpResponse *pResp);
};//End Class .
#endif
