
/************************************************
*
*Creator : Thong LT
*Email   : letrthong@gmail.com
*Website : http://letrthong.blogspot.com
*
*************************************************/
#ifndef INTERFACE_JSON_API_H_
#define INTERFACE_JSON_API_H_

#include  <string>

class iJsonApi
{
public:
	//virtual iJsonApi(std::string json) = 0;
    virtual std::string doGet() = 0;

    //Using delete-new
    virtual ~iJsonApi(){};
};
#endif
