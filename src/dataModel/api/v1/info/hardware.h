
#ifndef API_HARDWARE_H_
#define API_HARDWARE_H_

#include  "../../../../iJsonApi.h"

class hardware :public virtual iJsonApi {
public:	
	hardware(std::string json);
	~hardware();

	std::string doGet();
};
#endif