
#ifndef API_IP_H_
#define API_IP_H_

#include  "../../../../iJsonApi.h"

class ip :public virtual iJsonApi {
public:	
	ip(std::string json);
	~ip();

	std::string doGet();
private:
	std::string _ip;
};
#endif
