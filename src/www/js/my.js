/**************************************

Define ID 
***********************************/


/**************************************

***********************************/

function _log(str)
{
	console.log(str);
}

/**************************************
Using for Text, div, button
	setHight(int)
	setWidth(int)
	setPosstion(int,int)
	setColor(string)
	setBackgroundColor(string)
	setBorder(string)
***********************************/
function stypeTxtBn(id)
{
	this._stype=document.getElementById(id);
	if(this._stype == null)
	{
		_log("_stype is null");
	}
	//30
	this.setHight=function(h){
		var _h = h.toString() + "px"; 
		this._stype.style.height = _h;
	};
	//30
	this.setWidth=function(w){
		var _w = w.toString() + "px"; 
		this._stype.style.width = _w;
	};
	//30 30
	this.setPosstion=function(x,y){
		this._stype.style.position ="absolute"; 
		var _x = x.toString() + "px"; 
		var _y = y.toString() + "px"; 
		this._stype.style.left=_x;
		this._stype.style.top=_y;
	};
	//"red"
	this.setColor=function(cl){
		this._stype.style.color =cl;
	};
	//"red"
	this.setBackgroundColor=function(cl){
		this._stype.style.background =cl;
	};
	// "groove" 
	this.setBorder=function(t){
		this._stype.style.border =t;
		this._stype.style.borderWidth = "thin";
	};

	//"LightGrey"
	this.setBorderColor=function(t){
		this._stype.style.borderColor=t;

	};
}

/**************************************
Using for image
	setHight(int)
	setWidth(int)
	setPosstion(int,int)
	setSrc(string)
***********************************/
function stypeImge(id)
{
	this._stype= document.getElementById(id);
	if(this._stype == null)
	{
		_log("_stype is null");
	}
	//30
	this.setHight=function(h){
		this._stype.height = h;
	};
	//30
	this.setWidth=function(w){
		this._stype.width = w;
	};
	//30 30
	this.setPosstion=function(x,y){
		this._stype.style.position ="absolute"; 
		var _x = x.toString() + "px"; 
		var _y = y.toString() + "px"; 
		this._stype.style.left=_x;
		this._stype.style.top=_y;
	};
	//"Src"
	this.setSrc=function(path){
		this._stype.src =path;
	};
}

/******************************************
*
*******************************************/
function nodeButton(id)
{
	this._has = true;
	if(document.getElementById(id) == null){
		this._node =document.createElement("BUTTON");
		this._node.setAttribute("id",id);

		this.t1 = document.createTextNode("")
		this._node.appendChild(this.t1);
		this._has  = false;
	}
	//@commnet
	this.getNode=function(){
		if(this._has  == true){
			return null;
		}

		return this._node;
	};
	//@commnet
	this.setTxt=function(txt){
		if(this._has == false ){
			this.t1.nodeValue = txt;
		}else{

		}
	};
}
/******************************************
*
*******************************************/
function nodeImage(id)
{
	this._has = true;
	if(document.getElementById(id) == null){
		this._node = document.createElement("IMG");
		this._node.setAttribute("id",id);
		this._has  = false;
	}
	//@commnet
	this.getNode=function(){
		if(this._has  == true)
		{
			return null;
		}
		return this._node;
	};
}

/******************************************
*
*******************************************/
function nodeText(id)
{
	this._has = true;
	if(document.getElementById(id) == null){
		this._node = document.createElement("div");
		this._node.setAttribute("id",id);
		this.txt = document.createTextNode("");
		this._node.appendChild(this.txt);
		this._has  = false;
	}
	//@commnet
	this.getNode=function(){
		if(this._has  == true)
		{
			return null;
		}
		return this._node;
	};
	//@commnet
	this.setTxt=function(txt){
		if(this._has == false )
		{
			this.txt.nodeValue = txt;
		}
		else
		{

		}
	};
}


/******************************************
*
*******************************************/

function nodeTable(id)
{
	this._has = true;
	if(document.getElementById(id) == null){
		this._node = document.createElement("TABLE");
		this._node.setAttribute("id",id);
		this._has  = false;

	}
	//@commnet
	this.getNode=function(){
		if(this._has  == true){
			return null;
		}
		return this._node;
	};
	//@commnet
	this.addLine=function(line1,line2){
	    var tr = document.createElement("TR");

	 	var x = document.createElement("TD");
	 	var txt1 = document.createTextNode(line1);
		 x.appendChild(txt1);

	 	var y= document.createElement("TD");
	 	var txt2= document.createTextNode(line2);
		 y.appendChild(txt2);

		tr.appendChild(x);
		tr.appendChild(y);

		this._node.appendChild(tr);
	};
}

/******************************************
*
*******************************************/
function nodeToggleSwitch(id1,id2)
{    
	this._id1 = id1;
	this._id2 = id2;
	this._node = document.createElement("div");
	this._node.setAttribute("class","btn-group");

	var x = document.createElement("BUTTON");
	x.setAttribute("class","btn btn-primary");
	x.setAttribute("id",id1);
	var _txt1 = document.createTextNode(".");
	x.appendChild(_txt1);

	var y = document.createElement("BUTTON");
	y.setAttribute("class","btn btn-primary");
	y.setAttribute("id",id2);
	var _txt2 = document.createTextNode("OFF");
	y.appendChild(_txt2);

	this._node.appendChild(y);
	this._node.appendChild(x);

	//@commnet
	this.getNode=function(){
		if(this._has  == true){
			return null;
		}
		return this._node;
	};
     
	//@commnet
	this.getTxt=function(){
		return  _txt1.nodeValue ;
	}

	this.setTxt1=function(txt){
		_txt1.nodeValue = txt;
	}

	this.setTxt2=function(txt){
		_txt2.nodeValue = txt;
	}
	
}
/******************************************
*add event listener 
*******************************************/
function docAddEvent(id,callback)
{
	var el = document.getElementById(id);
	if( el != null)
	{
		el.addEventListener("click", callback, false);
	}
	else
	{
		_log(id +"is  null");
	}
}

/******************************************
* Create led
*******************************************/
function createLed(id,id_btn){

 	var _id_btn_1 = id_btn + "_1_led";
 	var _id_btn_2 = id_btn + "_2_led";
 	var _id_div   = id_btn + "_div_led";
 	var _id_txt   = id_btn + "_txt_led";
 	var _id_img   = id_btn + "_img_led";

	var _div=document.createElement("div");
	_div.setAttribute("id",_id_div);

	var _bt = new nodeToggleSwitch(_id_btn_1,_id_btn_2);
	_div.appendChild(_bt.getNode());

	var _img2 = new nodeImage(_id_img) ;
	_div.appendChild(_img2.getNode());

	var _txt = new nodeText(_id_txt);
	_div.appendChild(_txt.getNode());
	_txt.setTxt("chichen Room");

	document.getElementById(id).appendChild(_div);

	var _stypeDiv = new stypeTxtBn(_id_div);
	_stypeDiv.setHight(150);
	_stypeDiv.setWidth(150);
	_stypeDiv.setPosstion(5,5);
	_stypeDiv.setBorder("solid");
	_stypeDiv.setBorderColor("SlateGrey");

	var _stypeTxt = new stypeTxtBn(_id_txt);
	_stypeTxt.setHight(15);
	_stypeTxt.setWidth(120);
	_stypeTxt.setPosstion(10,0);
	_stypeTxt.setColor("LightSlateGray");

    var _stypeImg= new stypeImge(_id_img);
	_stypeImg.setHight(90);
	_stypeImg.setWidth(90);
	_stypeImg.setPosstion(20,20);
	_stypeImg.setSrc("./img/buld_off.jpeg");

	var _stypeBt1 = new stypeTxtBn(_id_btn_1);
	_stypeBt1.setHight(30);
	_stypeBt1.setWidth(70);
	_stypeBt1.setPosstion(74,100);
	_stypeBt1.setBackgroundColor("LightGrey");
	_stypeBt1.setBorderColor("LightGrey");
	_stypeBt1.setColor("LightGrey");

	var _stypeBt2 = new stypeTxtBn(_id_btn_2);
	_stypeBt2.setHight(30);
	_stypeBt2.setWidth(70);
	_stypeBt2.setPosstion(3,100);
	_stypeBt2.setBackgroundColor("LightSlateGray");
	_stypeBt2.setBorderColor("LightSlateGray");
	_stypeBt2.setColor("white");

	//@commnet
	this.setPosstion =function(x,y){
		_stypeDiv.setPosstion(x,y);
	};
	//@commnet
	this.setName =function(name){
		_txt.setTxt(name);
	};

	docAddEvent(_id_btn_1,clickLed);
	docAddEvent(_id_btn_2,clickLed);

	this.clickEvent=function(callback){
		docAddEvent(_id_btn_1,callback);
		docAddEvent(_id_btn_2,callback);
	};

	this.turnOn=function(){
		_bt.setTxt1("ON");
		_bt.setTxt2(".");

		 _stypeImg.setSrc("./img/bulb_on.gif");

		_stypeBt1.setBackgroundColor("LightSlateGray"); 
		_stypeBt1.setBorderColor("LightSlateGray");
		_stypeBt1.setColor("white");

		_stypeBt2.setBackgroundColor("LightGrey");
		_stypeBt2.setBorderColor("LightGrey");
		_stypeBt2.setColor("LightGrey");
	};
	this.turnOff=function(){
		_bt.setTxt1(".");
		_bt.setTxt2("OFF");

		_stypeImg.setSrc("./img/buld_off.jpeg");

		_stypeBt1.setBackgroundColor("LightGrey");
		_stypeBt1.setBorderColor("LightGrey");
		_stypeBt1.setColor("LightGrey");

		_stypeBt2.setBackgroundColor("LightSlateGray");
		_stypeBt2.setBorderColor("LightSlateGray");
		_stypeBt2.setColor("white");
	};
}//En function

function clickLed(){
	var currentdate = new Date();
	_log("clickEvent= " + currentdate);
	_log("id="+this.id);
	
	var n = this.id.search("_1_led");
	if(n <= 0)
	{
		n = this.id.search("_2_led");
	}
	
	var id_btn=  this.id.substring(0,n);

	_log("id_btn="+id_btn);
	var _id_btn_1 =  id_btn + "_1_led";
 	var _id_btn_2 = id_btn + "_2_led";
 	var _id_div   = id_btn + "_div_led";
 	var _id_txt   = id_btn + "_txt_led";
 	var _id_img   = id_btn + "_img_led";

 	var _btn1 = document.getElementById(_id_btn_1);
 	var _btn2 = document.getElementById(_id_btn_2);

 	if( _btn1.firstChild.nodeValue == "ON")
    {
    	_btn1.firstChild.nodeValue = ".";
    	_btn2.firstChild.nodeValue = "OFF";
    	
    	_btn1.style.background ="LightGrey" ;
    	_btn2.style.background ="LightSlateGray" ;
    	 
    	_btn1.style.color  ="LightGrey";
    	_btn2.style.color  ="white";

    	document.getElementById(_id_img).src = "./img/buld_off.jpeg";
    }
    else
    {
    	_btn1.firstChild.nodeValue = "ON";
    	_btn2.firstChild.nodeValue = ".";
    	
    	_btn1.style.background ="LightSlateGray" ;
    	_btn2.style.background ="LightGrey" ;

    	_btn1.style.color  ="white";
    	_btn2.style.color  ="LightGrey";

    	document.getElementById(_id_img).src = "./img/bulb_on.gif";
    }
}

/******************************************
* Create Pump
*******************************************/
function createPump(id,id_btn)
{
	var _id_btn_1 = id_btn + "_1_pump";
 	var _id_btn_2 = id_btn + "_2_pump";
 	var _id_div   = id_btn + "_div_pump";
 	var _id_txt   = id_btn + "_txt_pump";
 	var _id_img   = id_btn + "_img_pump";

	var _div=document.createElement("div");
	_div.setAttribute("id",_id_div);

	var _bt = new nodeToggleSwitch(_id_btn_1,_id_btn_2);
	_div.appendChild(_bt.getNode());

	var _img2 = new nodeImage(_id_img) ;
	_div.appendChild(_img2.getNode());

	var _txt = new nodeText(_id_txt);
	_div.appendChild(_txt.getNode());
	_txt.setTxt("Water Pump ");

	document.getElementById(id).appendChild(_div);

	var _stypeDiv = new stypeTxtBn(_id_div);
	_stypeDiv.setHight(150);
	_stypeDiv.setWidth(150);
	_stypeDiv.setPosstion(5,5);
	_stypeDiv.setBorder("solid");
	_stypeDiv.setBorderColor("SlateGrey");

	var _stypeTxt = new stypeTxtBn(_id_txt);
	_stypeTxt.setHight(15);
	_stypeTxt.setWidth(120);
	_stypeTxt.setPosstion(10,0);
	_stypeTxt.setColor("LightSlateGray ");

    var _stypeImg= new stypeImge(_id_img);
	_stypeImg.setHight(90);
	_stypeImg.setWidth(90);
	_stypeImg.setPosstion(20,20);
	_stypeImg.setSrc("./img/pump_off.png");

	var _stypeBt1 = new stypeTxtBn(_id_btn_1);
	_stypeBt1.setHight(30);
	_stypeBt1.setWidth(70);
	_stypeBt1.setPosstion(74,100);
	_stypeBt1.setBackgroundColor("LightGrey");
	_stypeBt1.setBorderColor("LightGrey");
	_stypeBt1.setColor("LightGrey");

	var _stypeBt2 = new stypeTxtBn(_id_btn_2);
	_stypeBt2.setHight(30);
	_stypeBt2.setWidth(70);
	_stypeBt2.setPosstion(3,100);
	_stypeBt2.setBackgroundColor("LightSlateGray");
	_stypeBt2.setBorderColor("LightSlateGray");
	_stypeBt2.setColor("white");

	//@commnet
	this.setPosstion =function(x,y){
		_stypeDiv.setPosstion(x,y);
	};
	//@commnet
	this.setName =function(name){
		_txt.setTxt(name);
	};

	docAddEvent(_id_btn_1,clickPump);
	docAddEvent(_id_btn_2,clickPump);

	this.clickEvent=function(callback){
		docAddEvent(_id_btn_1,callback);
		docAddEvent(_id_btn_2,callback);
	};

	this.turnOn=function(){
		_bt.setTxt1("ON");
		_bt.setTxt2(".");

		 _stypeImg.setSrc("./img/pump_on.gif");

		_stypeBt1.setBackgroundColor("LightSlateGray"); 
		_stypeBt1.setBorderColor("LightSlateGray");
		_stypeBt1.setColor("white");

		_stypeBt2.setBackgroundColor("LightGrey");
		_stypeBt2.setBorderColor("LightGrey");
		_stypeBt2.setColor("LightGrey");
	};
	this.turnOff=function(){
		_bt.setTxt1(".");
		_bt.setTxt2("OFF");

		_stypeImg.setSrc("./img/pump_off.jpeg");

		_stypeBt1.setBackgroundColor("LightGrey");
		_stypeBt1.setBorderColor("LightGrey");
		_stypeBt1.setColor("LightGrey");

		_stypeBt2.setBackgroundColor("LightSlateGray");
		_stypeBt2.setBorderColor("LightSlateGray");
		_stypeBt2.setColor("white");
	};
}

function clickPump(){
	var currentdate = new Date();
	_log("clickEvent= " + currentdate);
	_log("id="+this.id);
	
	var n = this.id.search("_1_pump");
	if(n <= 0)
	{
		n = this.id.search("_2_pump");
	}
	
	var id_btn=  this.id.substring(0,n);

	_log("id_btn="+id_btn);
	var _id_btn_1 =  id_btn + "_1_pump";
 	var _id_btn_2 = id_btn + "_2_pump";
 	var _id_div   = id_btn + "_div_pump";
 	var _id_txt   = id_btn + "_txt_pump";
 	var _id_img   = id_btn + "_img_pump";

 	var _btn1 = document.getElementById(_id_btn_1);
 	var _btn2 = document.getElementById(_id_btn_2);

 	if( _btn1.firstChild.nodeValue == "ON")
    {
    	_btn1.firstChild.nodeValue = ".";
    	_btn2.firstChild.nodeValue = "OFF";
    	
    	_btn1.style.background ="LightGrey" ;
    	_btn2.style.background ="LightSlateGray" ;
    	 
    	_btn1.style.color  ="LightGrey";
    	_btn2.style.color  ="white";

    	document.getElementById(_id_img).src = "./img/pump_off.png";
    }
    else
    {
    	_btn1.firstChild.nodeValue = "ON";
    	_btn2.firstChild.nodeValue = ".";
    	
    	_btn1.style.background ="LightSlateGray" ;
    	_btn2.style.background ="LightGrey" ;

    	_btn1.style.color  ="white";
    	_btn2.style.color  ="LightGrey";

    	document.getElementById(_id_img).src = "./img/pump_on.gif";
    }
}

/******************************************
* Create Fan
*******************************************/
function createFan(id,id_btn)
{
	var _id_btn_1 = id_btn + "_1_fan";
 	var _id_btn_2 = id_btn + "_2_fan";
 	var _id_div   = id_btn + "_div_fan";
 	var _id_txt   = id_btn + "_txt_fan";
 	var _id_img   = id_btn + "_img_fan";

	var _div=document.createElement("div");
	_div.setAttribute("id",_id_div);

	var _bt = new nodeToggleSwitch(_id_btn_1,_id_btn_2);
	_div.appendChild(_bt.getNode());

	var _img2 = new nodeImage(_id_img) ;
	_div.appendChild(_img2.getNode());

	var _txt = new nodeText(_id_txt);
	_div.appendChild(_txt.getNode());
	_txt.setTxt("Electric Fan");

	document.getElementById(id).appendChild(_div);

	var _stypeDiv = new stypeTxtBn(_id_div);
	_stypeDiv.setHight(150);
	_stypeDiv.setWidth(150);
	_stypeDiv.setPosstion(5,5);
	_stypeDiv.setBorder("solid");
	_stypeDiv.setBorderColor("SlateGrey");

	var _stypeTxt = new stypeTxtBn(_id_txt);
	_stypeTxt.setHight(15);
	_stypeTxt.setWidth(120);
	_stypeTxt.setPosstion(10,0);
	_stypeTxt.setColor("LightSlateGray");

    var _stypeImg= new stypeImge(_id_img);
	_stypeImg.setHight(90);
	_stypeImg.setWidth(90);
	_stypeImg.setPosstion(20,20);
	_stypeImg.setSrc("./img/fan_off.png");

	var _stypeBt1 = new stypeTxtBn(_id_btn_1);
	_stypeBt1.setHight(30);
	_stypeBt1.setWidth(70);
	_stypeBt1.setPosstion(74,100);
	_stypeBt1.setBackgroundColor("LightGrey");
	_stypeBt1.setBorderColor("LightGrey");
	_stypeBt1.setColor("LightGrey");

	var _stypeBt2 = new stypeTxtBn(_id_btn_2);
	_stypeBt2.setHight(30);
	_stypeBt2.setWidth(70);
	_stypeBt2.setPosstion(3,100);
	_stypeBt2.setBackgroundColor("LightSlateGray");
	_stypeBt2.setBorderColor("LightSlateGray");
	_stypeBt2.setColor("white");

	//@commnet
	this.setPosstion =function(x,y){
		_stypeDiv.setPosstion(x,y);
	};
	//@commnet
	this.setName =function(name){
		_txt.setTxt(name);
	};

	docAddEvent(_id_btn_1,clickFan);
	docAddEvent(_id_btn_2,clickFan);

	this.clickEvent=function(callback){
		docAddEvent(_id_btn_1,callback);
		docAddEvent(_id_btn_2,callback);
	};

	this.turnOn=function(){
		_bt.setTxt1("ON");
		_bt.setTxt2(".");

		 _stypeImg.setSrc("./img/fan_on.gif");

		_stypeBt1.setBackgroundColor("LightSlateGray"); 
		_stypeBt1.setBorderColor("LightSlateGray");
		_stypeBt1.setColor("white");

		_stypeBt2.setBackgroundColor("LightGrey");
		_stypeBt2.setBorderColor("LightGrey");
		_stypeBt2.setColor("LightGrey");
	};
	this.turnOff=function(){
		_bt.setTxt1(".");
		_bt.setTxt2("OFF");

		_stypeImg.setSrc("./img/fan_off.png");

		_stypeBt1.setBackgroundColor("LightGrey");
		_stypeBt1.setBorderColor("LightGrey");
		_stypeBt1.setColor("LightGrey");

		_stypeBt2.setBackgroundColor("LightSlateGray");
		_stypeBt2.setBorderColor("LightSlateGray");
		_stypeBt2.setColor("white");
	};
}

function clickFan(){
	var currentdate = new Date();
	_log("clickEvent= " + currentdate);
	_log("id="+this.id);
	
	var n = this.id.search("_1_fan");
	if(n <= 0)
	{
		n = this.id.search("_2_fan");
	}
	
	var id_btn=  this.id.substring(0,n);

	_log("id_btn="+id_btn);
	var _id_btn_1 =  id_btn + "_1_fan";
 	var _id_btn_2 = id_btn + "_2_fan";
 	var _id_div   = id_btn + "_div_fan";
 	var _id_txt   = id_btn + "_txt_fan";
 	var _id_img   = id_btn + "_img_fan";

 	var _btn1 = document.getElementById(_id_btn_1);
 	var _btn2 = document.getElementById(_id_btn_2);

 	if( _btn1.firstChild.nodeValue == "ON")
    {
    	_btn1.firstChild.nodeValue = ".";
    	_btn2.firstChild.nodeValue = "OFF";
    	
    	_btn1.style.background ="LightGrey" ;
    	_btn2.style.background ="LightSlateGray" ;
    	 
    	_btn1.style.color  ="LightGrey";
    	_btn2.style.color  ="white";

    	document.getElementById(_id_img).src = "./img/fan_off.png";
    }
    else
    {
    	_btn1.firstChild.nodeValue = "ON";
    	_btn2.firstChild.nodeValue = ".";
    	
    	_btn1.style.background ="LightSlateGray" ;
    	_btn2.style.background ="LightGrey" ;

    	_btn1.style.color  ="white";
    	_btn2.style.color  ="LightGrey";

    	document.getElementById(_id_img).src = "./img/fan_on.gif";
    }
}

/******************************************
* Create water Valve
*******************************************/
function createWaterValve(id,id_btn)
{
		var _id_btn_1 = id_btn + "_1_valve";
 	var _id_btn_2 = id_btn + "_2_valve";
 	var _id_div   = id_btn + "_div_valve";
 	var _id_txt   = id_btn + "_txt_valve";
 	var _id_img   = id_btn + "_img_valve";

	var _div=document.createElement("div");
	_div.setAttribute("id",_id_div);

	var _bt = new nodeToggleSwitch(_id_btn_1,_id_btn_2);
	_div.appendChild(_bt.getNode());

	var _img2 = new nodeImage(_id_img) ;
	_div.appendChild(_img2.getNode());

	var _txt = new nodeText(_id_txt);
	_div.appendChild(_txt.getNode());
	_txt.setTxt("Water Valve ");

	document.getElementById(id).appendChild(_div);

	var _stypeDiv = new stypeTxtBn(_id_div);
	_stypeDiv.setHight(150);
	_stypeDiv.setWidth(150);
	_stypeDiv.setPosstion(5,5);
	_stypeDiv.setBorder("solid");
	_stypeDiv.setBorderColor("SlateGrey");

	var _stypeTxt = new stypeTxtBn(_id_txt);
	_stypeTxt.setHight(15);
	_stypeTxt.setWidth(120);
	_stypeTxt.setPosstion(10,0);
	_stypeTxt.setColor("LightSlateGray");

    var _stypeImg= new stypeImge(_id_img);
	_stypeImg.setHight(90);
	_stypeImg.setWidth(90);
	_stypeImg.setPosstion(20,20);
	_stypeImg.setSrc("./img/valve_off.png");

	var _stypeBt1 = new stypeTxtBn(_id_btn_1);
	_stypeBt1.setHight(30);
	_stypeBt1.setWidth(70);
	_stypeBt1.setPosstion(74,100);
	_stypeBt1.setBackgroundColor("LightGrey");
	_stypeBt1.setBorderColor("LightGrey");
	_stypeBt1.setColor("LightGrey");

	var _stypeBt2 = new stypeTxtBn(_id_btn_2);
	_stypeBt2.setHight(30);
	_stypeBt2.setWidth(70);
	_stypeBt2.setPosstion(3,100);
	_stypeBt2.setBackgroundColor("LightSlateGray");
	_stypeBt2.setBorderColor("LightSlateGray");
	_stypeBt2.setColor("white");

	//@commnet
	this.setPosstion =function(x,y){
		_stypeDiv.setPosstion(x,y);
	};
	//@commnet
	this.setName =function(name){
		_txt.setTxt(name);
	};

	docAddEvent(_id_btn_1,clickValve);
	docAddEvent(_id_btn_2,clickValve);

	this.clickEvent=function(callback){
		docAddEvent(_id_btn_1,callback);
		docAddEvent(_id_btn_2,callback);
	};

	this.turnOn=function(){
		_bt.setTxt1("ON");
		_bt.setTxt2(".");

		 _stypeImg.setSrc("./img/valve_on.gif");

		_stypeBt1.setBackgroundColor("LightSlateGray"); 
		_stypeBt1.setBorderColor("LightSlateGray");
		_stypeBt1.setColor("white");

		_stypeBt2.setBackgroundColor("LightGrey");
		_stypeBt2.setBorderColor("LightGrey");
		_stypeBt2.setColor("LightGrey");
	};
	this.turnOff=function(){
		_bt.setTxt1(".");
		_bt.setTxt2("OFF");

		_stypeImg.setSrc("./img/valve_off.png");

		_stypeBt1.setBackgroundColor("LightGrey");
		_stypeBt1.setBorderColor("LightGrey");
		_stypeBt1.setColor("LightGrey");

		_stypeBt2.setBackgroundColor("LightSlateGray");
		_stypeBt2.setBorderColor("LightSlateGray");
		_stypeBt2.setColor("white");
	};
}

function clickValve(){
	var currentdate = new Date();
	_log("clickEvent= " + currentdate);
	_log("id="+this.id);
	
	var n = this.id.search("_1_valve");
	if(n <= 0)
	{
		n = this.id.search("_2_valve");
	}
	
	var id_btn=  this.id.substring(0,n);

	_log("id_btn="+id_btn);
	var _id_btn_1 = id_btn + "_1_valve";
 	var _id_btn_2 = id_btn + "_2_valve";
 	var _id_div   = id_btn + "_div_valve";
 	var _id_txt   = id_btn + "_txt_valve";
 	var _id_img   = id_btn + "_img_valve";

 	var _btn1 = document.getElementById(_id_btn_1);
 	var _btn2 = document.getElementById(_id_btn_2);

 	if( _btn1.firstChild.nodeValue == "ON")
    {
    	_btn1.firstChild.nodeValue = ".";
    	_btn2.firstChild.nodeValue = "OFF";
    	
    	_btn1.style.background ="LightGrey" ;
    	_btn2.style.background ="LightSlateGray" ;
    	 
    	_btn1.style.color  ="LightGrey";
    	_btn2.style.color  ="white";

    	document.getElementById(_id_img).src = "./img/valve_off.png";
    }
    else
    {
    	_btn1.firstChild.nodeValue = "ON";
    	_btn2.firstChild.nodeValue = ".";
    	
    	_btn1.style.background ="LightSlateGray" ;
    	_btn2.style.background ="LightGrey" ;

    	_btn1.style.color  ="white";
    	_btn2.style.color  ="LightGrey";

    	document.getElementById(_id_img).src = "./img/valve_on.gif";
    }
}

/******************************************
* Create socket
*******************************************/
function createSocket(id,id_btn)
{
	var _id_btn_1 = id_btn + "_1_socket";
 	var _id_btn_2 = id_btn + "_2_socket";
 	var _id_div   = id_btn + "_div_socket";
 	var _id_txt   = id_btn + "_txt_socket";
 	var _id_img   = id_btn + "_img_socket";

	var _div=document.createElement("div");
	_div.setAttribute("id",_id_div);

	var _bt = new nodeToggleSwitch(_id_btn_1,_id_btn_2);
	_div.appendChild(_bt.getNode());

	var _img2 = new nodeImage(_id_img) ;
	_div.appendChild(_img2.getNode());

	var _txt = new nodeText(_id_txt);
	_div.appendChild(_txt.getNode());
	_txt.setTxt("Electric Socket");

	document.getElementById(id).appendChild(_div);

	var _stypeDiv = new stypeTxtBn(_id_div);
	_stypeDiv.setHight(150);
	_stypeDiv.setWidth(150);
	_stypeDiv.setPosstion(5,5);
	_stypeDiv.setBorder("solid");
	_stypeDiv.setBorderColor("SlateGrey");

	var _stypeTxt = new stypeTxtBn(_id_txt);
	_stypeTxt.setHight(15);
	_stypeTxt.setWidth(120);
	_stypeTxt.setPosstion(10,0);
	_stypeTxt.setColor("LightSlateGray");

    var _stypeImg= new stypeImge(_id_img);
	_stypeImg.setHight(90);
	_stypeImg.setWidth(90);
	_stypeImg.setPosstion(20,20);
	_stypeImg.setSrc("./img/socket.jpeg");

	var _stypeBt1 = new stypeTxtBn(_id_btn_1);
	_stypeBt1.setHight(30);
	_stypeBt1.setWidth(120);
	_stypeBt1.setPosstion(23,100);
	_stypeBt1.setBackgroundColor("LightSlateGray");
	_stypeBt1.setBorderColor("LightGrey");
	_stypeBt1.setColor("LightGrey");
	_stypeBt1.setColor("white");

	var _stypeBt2 = new stypeTxtBn(_id_btn_2);
	_stypeBt2.setHight(30);
	_stypeBt2.setWidth(14);
	_stypeBt2.setPosstion(10,100);
	_stypeBt2.setBackgroundColor("LightSlateGray");
	//Turn off
	_stypeBt2.setBackgroundColor("#5c5c3d");
	_stypeBt2.setBorderColor("LightSlateGray");
	_stypeBt2.setColor("white");
    
    var _btn1 = document.getElementById(_id_btn_1);
     _btn1.firstChild.nodeValue = "OFF";

     var _btn2 = document.getElementById(_id_btn_2);
     _btn2.firstChild.nodeValue = ".";
	//@commnet
	this.setPosstion =function(x,y){
		_stypeDiv.setPosstion(x,y);
	};
	//@commnet
	this.setName =function(name){
		_txt.setTxt(name);
	};

	docAddEvent(_id_btn_1,clickSocket);
	docAddEvent(_id_btn_2,clickSocket);

	this.clickEvent=function(callback){
		docAddEvent(_id_btn_1,callback);
		docAddEvent(_id_btn_2,callback);
	};

	this.turnOn=function(){
		_bt.setTxt1("ON");
		_stypeBt2.setBackgroundColor("red");
		_stypeBt2.setColor("red");
		_stypeImg.setSrc("./img/socket_on.jpeg");
	};
	this.turnOff=function(){
		
		_bt.setTxt1("OFF");
		_stypeBt2.setBackgroundColor("#5c5c3d");
		_stypeBt2.setColor("#5c5c3d");
		_stypeImg.setSrc("./img/socket.jpeg");
	};
}

function clickSocket(){
	var currentdate = new Date();
	_log("clickEvent= " + currentdate);
	_log("id="+this.id);
	
	var n = this.id.search("_1_socket");
	if(n <= 0)
	{
		n = this.id.search("_2_socket");
	}
	
	var id_btn=  this.id.substring(0,n);

	_log("id_btn=" + id_btn);
	var _id_btn_1 = id_btn + "_1_socket";
 	var _id_btn_2 = id_btn + "_2_socket";
 	var _id_div   = id_btn + "_div_socket";
 	var _id_txt   = id_btn + "_txt_socket";
 	var _id_img   = id_btn + "_img_socket";

 	var _btn1 = document.getElementById(_id_btn_1);
 	var _btn2 = document.getElementById(_id_btn_2);

 	if( _btn1.firstChild.nodeValue == "ON")
    {
    	_btn1.firstChild.nodeValue = "OFF";
    	_btn1.style.background = "LightSlateGray" ;
    	_btn2.style.background = "#5c5c3d" ;
    	_btn2.style.color  = "#5c5c3d";
    	document.getElementById(_id_img).src = "./img/socket.jpeg";
    }
    else
    {
    	_btn1.firstChild.nodeValue = "ON";
    	_btn1.style.color  = "LightGrey";
    	_btn1.style.background = "LightSlateGray" ;
    	//Turn on
    	_btn2.style.background = "red" ;
    	_btn2.style.color  = "red";
    	document.getElementById(_id_img).src = "./img/socket_on.jpeg";
    }
}

/******************************************
* Test
*******************************************/
function testTable(id){
    var _tb = new nodeTable("table");
    document.getElementById(id).appendChild(_tb.getNode());

     _tb.addLine("Mac Address", "AB:1F:45:67:BC:01");
	_tb.addLine("Public IP", "103.205.152.154");
	_tb.addLine("Local IP","192.168.0.11");
	_tb.addLine("OS", "Linux");
	_tb.addLine("HWID","12343434343");
	_tb.addLine("RAM", 1);
	_tb.addLine("ROM",12);

    document.getElementById("table").style.width = "50%";
}

function testLed(id){	
	_log("hello world");

	var bt1= new createLed(id,"id_bt_1");
	bt1.turnOn();

	var bt2= new createLed(id,"id_bt_2");
	bt2.setPosstion(180,5);
	bt2.setName("Living Room");


	var bt3= new createPump(id,"id_bt_3");
	bt3.setPosstion(10,200);

	var bt4= new createFan(id,"id_bt_4");
	bt4.setPosstion(180,200);
	

	var bt5 = new createWaterValve(id,"id_bt_5");
	bt5.setPosstion(10,400);

	var bt6 = new createSocket(id,"id_bt_6");
	bt6.setPosstion(180,400);
	bt6.turnOn();
}

