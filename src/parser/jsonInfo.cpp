
#include <stdio.h>
#include <iostream>
using namespace std;

#include "jsonInfo.h"
#include "JsonUtils.h"
#include <json/json.h>

jsonInfo::jsonInfo()
{
   _key1 = "User";
   _key2 = "Id";

   _value1 = "";
   _value2 = 0;
   _iData= "";
   _oData = "";
}

jsonInfo::~jsonInfo()
{
}

int jsonInfo::parseJson(std::string iData)
{
   //JSON Validator
   _iData = iData;
   int ret = 0;
   json_object * jobj = json_tokener_parse((char*) _iData.data());

   enum json_type type = json_object_get_type(jobj);
   if (type == json_type_object)
   {
      json_object_object_foreach(jobj, key, val)
      {
         switch (json_object_get_type(val))
         {
            case json_type_null:
               break;
            case json_type_boolean: 
               break;
            case json_type_double: 
               break;
            case json_type_int:
               JsonUtils::json_parse_int(val, _key2, std::string(key), _value2);
               break;
            case json_type_object:
               break;
            case json_type_array:
               break;
            case json_type_string:
               JsonUtils::json_parse_string(val, _key1, std::string(key), _value1);
               break;
             default:
               break;
         }
      }
   }

   //Release memory

   return ret;
}

int jsonInfo::toJson(std::string &oData)
{
   //Creating a json object
   json_object *jobj = json_object_new_object();

   //Creating a json string
   json_object *jstring = json_object_new_string(getValue1().c_str());
   //Creating a json integer
   json_object *jint = json_object_new_int(getValue2());

   json_object_object_add(jobj, getKey1().c_str(), jstring);
   json_object_object_add(jobj, getKey2().c_str(), jint);

   _oData  = std::string(json_object_to_json_string(jobj));
    oData = _oData;
   //Release memory
   json_object_put(jstring);
   json_object_put(jint);
   json_object_put(jobj);
   return 0;
}

void jsonInfo::setKey1(std::string key1)
{
   _key1 = key1;
}

std::string jsonInfo::getKey1()
{
   return _key1;
}

void jsonInfo::setValue1(std::string value1)
{
   _value1 = value1;
}

std::string jsonInfo::getValue1()
{
   return  _value1;
}

void jsonInfo::setKey2(std::string key2)
{
   _key2 = key2;
}

std::string jsonInfo::getKey2()
{
   return _key2;
}

void jsonInfo::setValue2(int value2)
{
   _value2 = value2;
}

int jsonInfo::getValue2()
{
   return _value2;
}