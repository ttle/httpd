
#include <string>
#include <string.h>

/*
{
  "User": "user",
  "Id" : 123
}
*/
class jsonInfo
{
public:
   jsonInfo();
   ~jsonInfo();

   int parseJson(std::string iData);
   int toJson(std::string &oData);

   void setKey1(std::string iKey1);
   std::string getKey1();

   void setValue1(std::string iValue1);
   std::string getValue1();

   void setKey2(std::string iKey2);
   std::string getKey2();

   void setValue2(int iValue2);
   int getValue2();

private:
   std::string  _iData;
   std::string  _oData;

   std::string _key1;
   std::string _value1;

   std::string _key2;
   int  _value2;
};

