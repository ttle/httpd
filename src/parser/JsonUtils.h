
#include <json/json.h>
#include <string>

class JsonUtils
{
public:
   JsonUtils();
   ~JsonUtils();
   static int json_parse_string(json_object * iJobj, const std::string iKey,std::string jKey, std::string& oValue);
   static int json_parse_int(json_object * iJobj, const std::string iKey, std::string jKey, int& oValue);
};

