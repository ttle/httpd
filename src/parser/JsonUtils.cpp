#include "JsonUtils.h"
#include <iostream>
using namespace std;


JsonUtils::JsonUtils()
{
}

JsonUtils::~JsonUtils()
{
}

int JsonUtils::json_parse_string(json_object * iJobj, const std::string iKey, std::string jKey, std::string& oValue)
{
   if (iKey == jKey)
   {
      oValue = json_object_get_string(iJobj);
   }
   return 0;
}

int JsonUtils::json_parse_int(json_object * iJobj, const std::string iKey, std::string jKey, int& oValue)
{
   if (iKey == jKey)
   {
      oValue = json_object_get_int(iJobj);
   }
   return 0;
}