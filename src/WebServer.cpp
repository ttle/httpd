/************************************************
*
*Creator : Thong LT
*Email   : letrthong@gmail.com
*Website : http://letrthong.blogspot.com
*
*************************************************/

#include <iostream>
#include <string.h>
#include <unistd.h>

#include "WebServer.h"
#include "dataModel/api/v1/info/hardware.h"
#include "dataModel/api/v1/info/ip.h"
#include "dataModel/api/v1/usr/login.h"
#include "iJsonApi.h"

 WebServer::WebServer()
 {
 }

WebServer::~WebServer()
{
}

int WebServer::doGet(httpRequest Req, httpResponse *pResp)
{
	 //pResp->setHttpBody("Hello world!");
    iJsonApi* pJsonApi = NULL;

   if( Req.getRequestURI() == "/api/v1/info/hardware")
   {
      pJsonApi = new hardware(Req.getBody()); 
   }else if (Req.getRequestURI() == "/api/v1/info/ip" )
   {
      pJsonApi = new ip(Req.getIPv4());
   }
  /* else if (Req.getRequestURI() == "/api/v1/usr/login" )
   {
       pJsonApi = new login(Req.getBody()); 
   }*/
 

   if( pJsonApi != NULL)
   {
      pResp->setHttpBody( pJsonApi->doGet());
      delete pJsonApi;
   }
  
   return 0;
}



