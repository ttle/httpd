mkdir -p  raspberrypi
mkdir -p  raspberrypi/lib

cp  httpd.out ./raspberrypi 
cp  run.sh  ./raspberrypi 
cp -r www  ./raspberrypi
cp -r config ./raspberrypi

cp ../../../usr/lib/raspberry/x32/libjson-c.so  ./raspberrypi/lib
cp ../../../usr/lib/raspberry/x32/libjson-c.so.3   ./raspberrypi/lib
cp ../../../usr/lib/raspberry/x32/libjson-c.so.3.0.0   ./raspberrypi/lib 

cp ../../../usr/lib/raspberry/x32/libssl.so   ./raspberrypi/lib
cp ../../../usr/lib/raspberry/x32/libssl.so.1.1  ./raspberrypi/lib

cp ../../../usr/lib/raspberry/x32/libcrypto.so   ./raspberrypi/lib
cp ../../../usr/lib/raspberry/x32/libcrypto.so.1.1   ./raspberrypi/lib

scp -r raspberrypi  pi@raspberrypi.local:/tmp

rm -r  raspberrypi/lib
rm -r raspberrypi
