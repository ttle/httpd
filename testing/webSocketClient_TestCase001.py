import websocket
import threading

#https://github.com/websocket-client/websocket-client
msg='{"messageID":"12345", "protocolID":"UHES2@1.0@SND", "priority":null, "desID":"aaa@mail.com", "sourceID":"123", "sessionID":Null,  "functionID":"FFFF","keyedHashChecksum":Null, "functionArgument":[{"argumentID":"ARG01","argumentValue":"0"}, {"argumentID":"ARG02","argumentValue":"unused2"},  {"argumentID":"ARG03","argumentValue":"unused3"}]}'

#msg="hello"
ws = websocket.WebSocket()
ws.connect("wss://neonplatform.herokuapp.com/services/uhesdevice/123")
print "sending data\n"
#ws.send(msg)
ws.send("hello world")
print("Receiving...")
result = ws.recv()
print("Received '%s'" % result)
ws.close()
